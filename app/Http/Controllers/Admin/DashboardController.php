<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use RealRashid\SweetAlert\Facades\Alert;

class DashboardController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware(['auth','verified']);
    }

    public function index()
    {
        $data['title'] = 'Dashboard | Turorial';
        $data['active'] = 'dashboard';

        Alert::success('Cheers!', 'Anda berada di dashboard.');
        return view('backend.dashboard', $data);
    }
}
