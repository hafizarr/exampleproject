<!-- Bootstrap core JavaScript-->
<script src="{{asset('vendor/backend')}}/vendor/jquery/jquery.min.js"></script>
<script src="{{asset('vendor/backend')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('vendor/backend')}}/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('vendor/backend')}}/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="{{asset('vendor/backend')}}/vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="{{asset('vendor/backend')}}/js/demo/chart-area-demo.js"></script>
<script src="{{asset('vendor/backend')}}/js/demo/chart-pie-demo.js"></script>

<script src="{{asset('vendor')}}/sweetalert/sweetalert.all.js"></script>
<script src="{{asset('vendor')}}/sweetalert/myscript.js"></script>

@include('sweetalert::alert')
@yield('pageScripts')